package sheridan;

public class Prime {

	public static boolean isPrime(int number) {
		for (int i = 2; i <= number / 2; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	// a method that returns 'is' or 'is not'
	public static String isAPrime(int num) {
		String a = "is";
		if (!isPrime(num))
			a += " not";
		return a;
	}

	public static void main(String[] args) {

		System.out.println("Number 29 is prime? " + Prime.isPrime(29));
		System.out.println("Number 29 is prime? " + Prime.isPrime(30));
		System.out.println("Number 33 is prime? " + Prime.isPrime(33));
		System.out.println("Number 34 is prime? " + Prime.isPrime(34));
		System.out.println("Number 35 is prime? " + Prime.isPrime(35));
		System.out.println("Number 36 is prime? " + Prime.isPrime(36));
		System.out.println("Number 100 is prime? " + Prime.isPrime(100)); // Added by Sahashra[figure changed by Binod]
		System.out.println("Number 81 is prime? " + Prime.isPrime(81)); // Made changed on the line added by Binod

		System.out.println("Please note that 41 " + (Prime.isPrime(41) ? "is" : "is not") + " a prime number.");
		System.out.println("Please note that 97 " + (Prime.isPrime(97) ? "is" : "is not") + " a prime number.");

		System.out.println("99 " + Prime.isAPrime(99) + " a prime number.");
	}

}
